/*
10. Update App.js
Akhirnya, untuk membuat aplikasi dan context kita berjalan dengan sempurna, kamu harus mengupdate App.js kamu yang berada di folder src menjadi seperti berikut.

Jika kamu menyadari, di mana kita menggunakan context ini? Oke, ini merupakan sesuatu di context. Context yang sudah kamu buat, kamu harus membungkus semua component yang ada di App.js dengan State, di dalam kasus kita, kita membungkus semua komponen menggunakan TodoState. Karena TodoState itu menerima children komponen di dalamnya, coba lihat file TodoState.js me-return apa.

Itu caranya kamu mengunakan context, kapanpun kamu membuat context baru untuk aplikasimu, kamu harus mendaftarkannya di App.js ini untuk membuat context ini berfungsi.
*/

// Styles
import "./style.css";

// Components
import TodoForm from "./components/TodoForm";
import TodoList from "./components/TodoList";

// State
import TodoState from "./context/TodoState";

function App() {
  return (
    // Wrap entire  app with State
    <TodoState>
      <div className="container flex flex-col mt-4">
        <h1 className="text-center">Todo App With Context</h1>
        {/* Todo Form */}
        <TodoForm />

        {/* Todo List */}
        <div className="flex flex-col mt-4">
          <TodoList />
        </div>
      </div>
    </TodoState>
  );
}

export default App;
