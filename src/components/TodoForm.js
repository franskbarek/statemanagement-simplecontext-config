/* 8. Membuat Form
Jika kamu sudah selesai dengan CSS sebelumnya, saatnya kita membuat komponen untuk membuat todo baru. Saya akan membuat folder di dalam folder src yang bernama components dan didalam folder components ini saya akan membuat file bernama TodoForm.js. Dan juga, di dalam komponen ini saya akan mengimplementasikan context yang sudah kita buat sebelumnya.*/

// Bring the useContext to using context
import React, { useContext } from "react";

//Context
import TodoContext from "../context/TodoContext";

const TodoForm = () => {
  const { setTodoTitle, createTodo, title } = useContext(TodoContext);

  const onCreateTodo = (e) => {
    e.preventDefault();

    if (title === "") {
      alert("Fill the form");
      return;
    }
    createTodo(title);
  };

  return (
    <form className="flex justify-center items-center mt-4" onSubmit={onCreateTodo}>
      <input type="text" name="title" className="input" onChange={(e) => setTodoTitle(e.target.value)} value={title} placeholder="Thing you wanna do..." />
      <button type="submit" className="btn">
        Save
      </button>
    </form>
  );
};

export default TodoForm;
