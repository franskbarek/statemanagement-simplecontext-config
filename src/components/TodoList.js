/*9. Membuat Komponent Untuk Data Todo
Setelah kita membuat TodoForm.js, kita akan membuat komponen yang akan berurusan dengan data todo-nya. Masih di dalam folder components, saya akan membuat file bernama TodoList.js. Dan juga, kita akan mengimplmentasikan context di dalamnya. 

Itu hanya komponen yang simpel, benarkan?? Tapi lihat, kita tidak berurusan dengan props lagi nih, yang berarti itu sangat bagus untuk menggunakan metode context ini. Kita hanya mendeklarasikan context kita dengan bantuan useContext() dari react dan bisa mengambil apapun yang ada di dalamnya. Ini merupakan kelebihan dari context ini!

*/

// Bring the useContext to using context
import React, { useEffect, useContext } from "react";

// Context
import TodoContext from "../context/TodoContext";

const TodoList = () => {
  const { getTodos, todos, loading, deleteTodo } = useContext(TodoContext);

  useEffect(() => {
    getTodos();
  }, []);

  return (
    <>
      {!loading &&
        todos &&
        todos.map((todo) => (
          <div className="flex flex-row justify-between items-center todo-item">
            <p>{todo.title}</p>
            <span className="remove-todo" onClick={() => deleteTodo(todo.id)}>
              &times;
            </span>
          </div>
        ))}
    </>
  );
};

export default TodoList;
