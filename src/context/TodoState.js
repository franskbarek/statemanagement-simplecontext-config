/*
## 6. Membuat State.
Di langkah ini, ini adalah akar dari si Context ini. Masih di dalam folder context, saya akan membuat file dengan nama TodoState.js. Di dalam file ini akan berisikan state-state, fungsi-fungsi, dan beberapa nilai yang akan digunakan oleh si komponen nantinya. Di sinilah kita membuat macam halnya seperti variable global dengan context
*/

import React, { useReducer } from "react";

// Bring the context
import TodoContext from "./TodoContext";

// Bring the reducer
import TodoReducer from "./TodoReducer";

// Bring the types
import { SET_TODO_TITLE, GET_TODOS, CREATE_TODO, DELETE_TODO, CLEAR_TODO_TITLE } from "./TodoTypes";

const TodoState = ({ children }) => {
  // Define our state
  const initialState = {
    todos: [],
    title: "",
    loading: true,
  };

  // Dispatch the reducer
  // This come from useReducer from ReactJS
  const [state, dispatch] = useReducer(TodoReducer, initialState);

  // Set the title for the new todo
  // This will change whenever user type in the from later
  const setTodoTitle = (payload) => {
    dispatch({ type: SET_TODO_TITLE, payload });
  };

  // Get todos
  const getTodos = async () => {
    try {
      const todos = await fetch("https://jsonplaceholder.typicode.com/todos?_limit=5");
      const toJSON = await todos.json();
      dispatch({ type: GET_TODOS, payload: toJSON });
    } catch (err) {
      console.log(err.message);
    }
  };

  // Create todo
  const createTodo = async (title) => {
    const newTodo = {
      title,
      completed: false,
    };

    try {
      const todo = await fetch("https://jsonplaceholder.typicode.com/todos", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(newTodo),
      });
      const toJSON = await todo.json();

      dispatch({ type: CLEAR_TODO_TITLE });
      dispatch({ type: CREATE_TODO, payload: toJSON });
    } catch (err) {
      console.log(err.message);
    }
  };

  // Delete Todo
  const deleteTodo = async (id) => {
    try {
      await fetch(`https://jsonplaceholder.typicode.com/todo/${id}`, {
        method: "DELETE",
      });
      dispatch({ type: DELETE_TODO, payload: id });
    } catch (err) {
      console.log(err.message);
    }
  };

  // Destruct the states
  const { todos, title, loading } = state;
  // Here's where we gonna use this state and funcitons to dealing with the context
  // The context will wrapping our entire application with this component and accept children in it
  // Anything state or function, must be passed in to value props in this provider in order to use it
  // NOTE: PLEASE NOTICE, IF YOU DIDN'T PASS THE STATE OR THE FUNCTION in THIS VALUE PROPS, YOU WILL GET AN ERROR
  return <TodoContext.Provider value={{ todos, title, loading, getTodos, setTodoTitle, createTodo, deleteTodo }}>{children}</TodoContext.Provider>;
};

export default TodoState;

/*Kode di atas sudah saya selesaikan untuk menampung keseluruhan fitur di aplikasi yang kita buat ini. State ini digunakan untuk mendeklarasikan state-state yang akan kita gunakan, fungsi-fungsi yang beritenraksi dengan server, dan beberapa logic untuk aplikasi ini. Sambil belajar-belajar bahasa inggris kan ya dari koding orang lain? haha, tenang! bahasa inggris di koding saya itu mudah di pahami ko hehehe. Silahkan di Translate saja :D.

Jika kamu ingin menambahkan fungsi maka kamu tambahkan di sini dan jangan membuat fungsi yang berinteraksi dengan server di komponen!

Apa yang dilakukan types di sini? ketika kamu ingin mengubah state maka kamu harus memanggil dispatch() yang sudah disediakan oleh useReducer() dari React. lihat baris no.28. dispatch() ini dia menerima 2 object di dalamnya, yaitu type dan payload.

Yap, kita sudah selesai nih membuat contextnya. perlu di ingat, ketika kamu membuat context, ada beberapa hal yang perlu di ingat, yaitu:

State.
Reducer.
Type.
CreateContext().
Nah, kita sudah membuat contextnya nih. Terus gimana cara kita menggunakannya? silahkan cek langkah ke 7!*/
