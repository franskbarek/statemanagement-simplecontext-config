/*
 ## 5. Membuat Reducer
Apa itu Reducer? Reducer di ReactJS mempunyai arti untuk menentukan state-state yang akan berubah di dalam aplikasi kita. Untuk kasus ini, jika kamu ingin mengubah state-state, maka kamu harus mengkontrol semuanya di sini.

Di dalam Reducer ini juga, saya akan mengimpor types yang sudah Saya buat di langkah nomor 4 di atas. Karena kita akan menjalankan beberapa fungsi yang akan diperintahkan menggunakan reducer ini.

Masih di dalam context folder, Saya akan membuat file yang dinamakan TodoReducer.js. Berikut kodenya.

*/

// Dont Forget import the types
import { SET_TODO_TITLE, GET_TODOS, CREATE_TODO, DELETE_TODO, CLEAR_TODO_TITLE } from "./TodoTypes";

export default function TodoReducer(state, { type, payload }) {
  switch (type) {
    // Get all todos
    case GET_TODOS:
      return {
        ...state,
        loading: false,
        todos: payload,
      };
    // Set Todo title for form
    case SET_TODO_TITLE:
      return {
        ...state,
        title: payload,
      };
    // Create a new todo
    case CREATE_TODO:
      return {
        ...state,
        todos: [payload, ...state.todos],
      };
    // Clear todo title after create
    case CLEAR_TODO_TITLE:
      return {
        ...state,
        title: "",
      };
    // Delete a todo
    case DELETE_TODO:
      return {
        ...state,
        todos: state.todos.filter((todo) => todo.id !== payload),
      };
    default:
      return state;
  }
}
