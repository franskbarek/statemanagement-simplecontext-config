/*
## 4. Membuat Types.
Di sini, kita akan membuat types, apa types itu? Di kasus yang kita hadapi, types ini berfungsi untuk mendefinisikan apa saja yang akan dilakukan oleh aplikasi kita? Seperti “Hey, aku mau mengambil data nih dari API ini, panggil type ini aja”.
*/

/**
 * @description   Types is just for constant that for dispatching our ACTION!! in order to reducing a Misspelling
 */

export const GET_TODOS = "GET_TODOS";
export const SET_TODO_TITLE = "SET_TODO_TITLE";
export const CLEAR_TODO_TITLE = "CLEAR_TODO_TITLE";
export const CREATE_TODO = "CREATE_TODO";
export const DELETE_TODO = "DELETE_TODO";
